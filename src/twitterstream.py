import sys
import oauth2 as oauth
import urllib2 as urllib

## See Assignment 1 instructions or README for how to get these credentials
access_token_key = "271007218-oXHxveJvKw3wJ4sTNAxjY3aoob1n7U2iRP4IAQ04"
access_token_secret = "R8NpMTheAnuwwQ7nT8VjmpmmNPWU2TMuoTrRazF5UQ"

consumer_key = "Yi5uqZsk4XYQue2V6LmWQ" 
consumer_secret = "t1fAeusc9mFijisIAGAuqZGycs8lt1Kn8CSYiDUOorY"

_debug = 0

oauth_token    = oauth.Token(key=access_token_key, secret=access_token_secret)
oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)

signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

http_method = "GET"


http_handler  = urllib.HTTPHandler(debuglevel=_debug)
https_handler = urllib.HTTPSHandler(debuglevel=_debug)

'''
Construct, sign, and open a twitter request
using the hard-coded credentials above.
'''
def twitterreq(url, method, parameters):
	req = oauth.Request.from_consumer_and_token(oauth_consumer,
					     token=oauth_token,
					     http_method=http_method,
					     http_url=url, 
					     parameters=parameters)

	req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)

	headers = req.to_header()

	if http_method == "POST":
		encoded_post_data = req.to_postdata()
	else:
		encoded_post_data = None
		url = req.to_url()

	opener = urllib.OpenerDirector()
	opener.add_handler(http_handler)
	opener.add_handler(https_handler)

	response = opener.open(url, encoded_post_data)

	return response

'''
Get csv delimited string from a file name string
Returns a string of the file contents (comma delimited values)
'''
def getwords(str):
	f = open(str, "r")
	csv_words = "" 
	for line in f:
		csv_words += line
	f.close()
	return csv_words


'''
Actually get the twitter data.
'''
def fetchsamples():
	#url = "https://stream.twitter.com/1/statuses/sample.json"
	url = "https://stream.twitter.com/1.1/statuses/filter.json" # twitter stream updated to 1.1
	
	## get a list of csv delimited words to filter for when grabbing tweets
	if len(sys.argv) != 2:
		print '''usage:
python twitterstream.py healthywords.csv > output.txt'''
		sys.exit(0)
	csv_words = getwords(sys.argv[1])

	## parameters:
	##	track: Keywords to track. Phrases of keywords are specified by a comma-separated list.
	#parameters = [("track", csv_words), ("locations", "-122.75,36.8,-121.75,37.8")] ## track=csv_words OR location=sanfrancisco
	parameters = [("track", csv_words)]

	## grab a twitter stream response and print each line out
	response = twitterreq(url, "POST", parameters)
	for line in response:
		print line.strip()

'''
Main function.
'''
if __name__ == '__main__':
	fetchsamples()





