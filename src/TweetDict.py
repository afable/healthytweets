"""
TweetDict class containts tweet data in a dictionary.
"""

import json

class TweetDict:
	tweets = [] ## a list of dictionaries containing the tweet data
	jsondata = [] ## a list for the data ['created_at', 'full_name', 'country', 'text']
	mapdata = [] ## a list for the final parsed data to be mapped

	created_at = 0
	country_code = 1
	full_name = 2
	text = 3

	''' class init '''
	def __init__(self):
		self.tweets = []
		return

	''' gettweet imports tweet data from a file name '''
	def gettweets(self, infilename):
		inf = open(infilename, "r")
		for line in inf:
			strdict = line.strip()
			tweetdict = json.loads(strdict)
			self.tweets.append(tweetdict)
		inf.close()
		return

	''' print each tweet in the tweets list in tabular dicionary, key format '''
	def printtweets(self):
		for tweet in self.tweets:
			self.print_tabbed_dict(tweet, 0)
			print '''--------------------------------------------------------------------------------------------------------------------------'''
		return



	''' print each tweet in the tweets list in tabular dicionary, key format '''
	def print_tabbed_dict(self, dictionary, tabs):
		if tabs > 1:
			return
		for key, value in dictionary.iteritems():
			if isinstance(value, unicode):
				print '\t'*tabs, key + ':'
				print '\t'*(tabs+1), value.encode('ascii', 'ignore')
			elif isinstance(value, dict):
				print '\t'*tabs, key + ':'
				self.print_tabbed_dict(value, tabs+1)
			else:
				print '\t'*tabs, key + ':'
				print '\t'*(tabs+1), value
		return

	''' process the tweets into the jsondata '''
	def generatejson(self):
		## a list for the data ['created_at', 'full_name', 'country', 'text']
		for tweet in self.tweets:
			tlist = ['', '', '', '']
			for key, value in tweet.iteritems():
				if key == 'created_at':
					time = value.encode('utf8')
					time = time[:-15]
					tlist[self.created_at] = time
				if key == 'place' and isinstance(value, dict):
					for pk, pv in value.iteritems():
						if pk == 'country_code':
							tlist[self.country_code] = pv.encode('utf8')
						if pk == 'full_name':
							tlist[self.full_name] = pv.encode('utf8')
				if key == 'text':
					tlist[self.text] = value.encode('utf8')
			self.jsondata.append(tlist)
		return

	''' parse to final json list with only us places '''
	def trimcountry(self, country_code):
		i = 0
		for line in self.jsondata:
			if line[self.country_code] == country_code:
				self.mapdata.append(line)
			i += 1
		return

	''' write out json to file '''
	def puttweets(self, outfile):
		f = open(outfile, 'w')
		for line in self.mapdata:
			f.write(json.dumps(line))
			f.write("\n")
		f.close()
		return









