"""
processtweets.py is the main program that grabs the streaming
 tweet data and formats it into a json file that can be
 map reduced.
"""

import sys
import TweetDict

########################################################
## Create objects
########################################################
td = TweetDict.TweetDict()


########################################################
## Get tweet data and parse it to an output file
########################################################
## Check that we have arguments for the input file and output file
if len(sys.argv) != 3:
	print '''usage:
python processtweets.py output.txt tout.json'''
	sys.exit()

## Load the json file and execute the TweetDict processing and MapReduce query which prints the result to stdout.
inputfilename = sys.argv[1]
outputfilename = sys.argv[2]
td.gettweets(inputfilename)

## Load tweets into jsondata
td.generatejson()

## Trim for a specific country code
td.trimcountry("US")

## Write to output file
td.puttweets(outputfilename)



